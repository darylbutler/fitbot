namespace bot {
    public static class Constants {
        // -- Settings
        public const int CacheSize = 100; // Messages to keep in the local cache
        public const int MessageRate = 20;
        public const int PollMinutes = 10;
        public const string DIR = @"/home/daryl/projects/fitbot/";
        public const string KEYFILE = DIR + "keyfile";
        public const string DATABASE = DIR + "database.db";
        public const string DATABASE_BACKUP_PATH = DIR + "dbbackups/";
        public const string LOG_PATH = DIR + "bot.log";
        public const string WOD_PATH = DIR + "src/wods";

        public const int DayStartMessageHour = 08;
        public const int DayMorningWorkoutEndHour = 12;
        public const int DayLunchWorkoutEndHour = 16;
        public const int DayAfternoonWorkoutEndHour = 20;
        public const int DaySummaryHour = 22;


        // -- Discord server constants
        public const ulong GuildID = 796909356556877824;
        // Channels
        public const ulong ChannelDiscussion = 796909356556877827;
        public const ulong ChannelGoals = 796909511977730048;
        public const ulong ChannelWorkouts = 796913224644558898;
        public const ulong ChannelTesting = 801609366964797450;
        // Roles
        public const ulong RoleMondayWorkout = 796918220727517244;
        public const ulong RoleTuesdayWorkout = 796918326655451167;
        public const ulong RoleWednesdayWorkout = 796918368694435881;
        public const ulong RoleThursdayWorkout = 796918385769709609;
        public const ulong RoleFridayWorkout = 796918413678608414;
        public const ulong RoleSaturdayWorkout = 796918428111339560;
        public const ulong RoleSundayWorkout = 796918458096812033;
        // Messages
        public const ulong MessageMondayWorkout = 796913138795937863;
        public const ulong MessageTuesdayWorkout = 796913145628983306;
        public const ulong MessageWednesdayWorkout = 796913154293760030;
        public const ulong MessageThursdayWorkout = 796913164330729533;
        public const ulong MessageFridayWorkout = 796913169262968845;
        public const ulong MessageSaturdayWorkout = 796913192407269417;
        public const ulong MessageSundayWorkout = 796913197818576897;
        public const ulong MessageMorningWorkout = 797136357096161300;
        public const ulong MessageLunchWorkout = 797136582371967046;
        public const ulong MessageAfternoonWorkout = 797136684814696488;
        public const ulong MessageMotivateMeOnMissedWorkout = 797138730673635368;
        public const ulong MessageMotivateMeOnMultipleMissedWorkouts = 797139132475768833;
        public const ulong MessageCallOutHittingGoals = 797139459635413023;
        // Message Keys
        public const string DayWorkoutMsgKey = "#TimeToWorkout";
        public const string DayMotivateMsgKey = "#Motivate";
        public const string DaySummaryMsgKey = "#OkayPutDownYourBarbells";
        public const string UserWorkoutMsgKey = "#Workout";

        public const ulong BotID = 796908837537185802;
    }
}