﻿using System;
using System.Collections.Generic;

namespace bot {
  public class MessageProvider {
    private readonly List<string> _messages;
    private readonly HashSet<int> _used;
    private readonly Random _rand;

    public MessageProvider(IEnumerable<string> msgs) {
      _messages = new List<string>(msgs);
      _used = new HashSet<int>(_messages.Count);
      _rand = new Random();
    }

    public void ResetUsed() {
      _used.Clear();
    }

    public string GetNext() {
      if (_messages.Count < 1)
        return string.Empty;
      
      if (_used.Count >= _messages.Count)
        ResetUsed();

      while (true) {
        var i = _rand.Next(0, _messages.Count - 1);

        if (_used.Add(i))
          return _messages[i];
      } 
    }
  }
}