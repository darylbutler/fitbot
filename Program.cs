﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;
using Serilog;

namespace bot {
    internal class Program : IDisposable {
        private static string Version;
        private static string BOTAPIKEY;
        private readonly DiscordSocketClient _client;
        private readonly ManualResetEventSlim BotNotBusy = new();
        private readonly ManualResetEventSlim Connected = new();
        private readonly Database Database;
        private readonly MessageProvider MorningPrefixMsgs, MorningSuffixMsgs, MotivateMsgs, StreakPosMsgs, StreakNegMsgs, SnarkMsgs;
        private readonly Random Random = new();
        private bool isDisposed;

        public Program() {
            var config = new DiscordSocketConfig {
                AlwaysDownloadUsers = true,
                MessageCacheSize = Constants.CacheSize
            };
            _client = new DiscordSocketClient(config);

            MorningPrefixMsgs = new MessageProvider(GenerateGoodMorningPrefix());
            MorningSuffixMsgs = new MessageProvider(GenerateGoodMorningSuffix());
            MotivateMsgs = new MessageProvider(GenerateMotivateMessage());
            StreakPosMsgs = new MessageProvider(GeneratePositiveStreakMessages());
            StreakNegMsgs = new MessageProvider(GenerateNegativeStreakMessages());
            SnarkMsgs = new MessageProvider(GenerateSnarkAtBotMessage());

            _client.Log += LogAsync;
            _client.Ready += ReadyAsync;
            _client.MessageReceived += MessageReceivedAsync;
            _client.MessageDeleted += MessageRemovedAsync;
            _client.MessageUpdated += MessageEditedAsync;
            _client.ReactionAdded += HandleReactionAddedAsync;
            _client.ReactionRemoved += HandleReactionRemovedAsync;
            _client.UserJoined += HandleUserJoined;

            _client.LoggedOut += () => {
                Log.Information("Logout Event!");
                Connected.Reset();
                BotNotBusy.Set();
                return Task.CompletedTask;
            };

            Database = new Database(Constants.DATABASE);
            Database.InitDatabase();
        }
        public void Dispose() {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private static void Main(string[] args) {
            // Get Version Info
            Version = Assembly.GetExecutingAssembly().GetType("GitVersionInformation").GetField("FullSemVer").GetValue(null).ToString();

            Log.Logger = new LoggerConfiguration()
                        .MinimumLevel.Debug()
                        .WriteTo.Console()
                        .WriteTo.File(Constants.LOG_PATH, retainedFileCountLimit: 3, fileSizeLimitBytes: 1024 * 1024 * 512)
                        .CreateLogger();
            Log.Information($"Starting up on {Version} ({GetVersionString():yyyy-MM-dd HH:mm:ss.fff}) -----------------------------------------");

            if (File.Exists(Constants.KEYFILE)) {
                BOTAPIKEY = File.ReadAllText(Constants.KEYFILE).Trim();
            } else {
                Log.Fatal("Error, Couldn't find key file!");
                return;
            }

            new Program().MainAsync().GetAwaiter().GetResult();
            Log.Information("Closing...");
            Log.CloseAndFlush();
        }
        private static DateTime GetVersionString() {
            try {
                var path = Assembly.GetEntryAssembly().Location;
                var file = new FileInfo(path);
                return file.CreationTime;
            } catch {
                return DateTime.MinValue;
            }
        }
        protected virtual void Dispose(bool disposing) {
            if (isDisposed) {
                return;
            }
            if (disposing) {
                // free managed resources
                _client?.Dispose();
            }

            isDisposed = true;
        }

        public async Task MainAsync() {
            Connected.Reset();
            await _client.LoginAsync(TokenType.Bot, BOTAPIKEY);
            await _client.StartAsync();

            BotNotBusy.Set();
            Connected.Wait();
            Startup();

            var timeWindow = TimeSpan.FromMinutes(Constants.PollMinutes);

            while (true) {
                // Sync to the next top of hour
                var timeDiff = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour,
                                            DateTime.Now.Minute / Constants.PollMinutes * Constants.PollMinutes, 0).AddMinutes(Constants.PollMinutes)
                             - DateTime.Now;
                await Task.Delay(timeDiff);

                var now = DateTime.Now;
                var dayStart = new DateTime(now.Year, now.Month, now.Day, Constants.DayStartMessageHour, 0, 0);
                var dayEnd = new DateTime(now.Year, now.Month, now.Day, Constants.DaySummaryHour, 0, 0);

                if (!BotNotBusy.Wait(5000)) {
                    continue;
                }
                try {
                    BotNotBusy.Reset();

                    if (IsTimeAfterTarget(DateTime.Now, dayStart, timeWindow) && Database.GetWorkoutDayStartMessage(DateTime.Now) < 1) {
                        // -- Day Start Message   
                        Log.Information("Creating today's message");
                        try {
                            CreateTodaysWorkoutMessage();
                        } catch (Exception ex) {
                            Log.Error(ex, "Error creating day start msg");
                        }
                        try {
                            await GetWorkoutsChannel()
                               .SendMessageAsync(
                                    $"`NEW!` Today's WOD (<3 {MentionUtils.MentionUser(124695806086217728)}):\n\n{GetRandomWod()}\n\n*You can also request a new one in channel or dm with `!wod`*");
                        } catch (Exception ex) {
                            Log.Error(ex, "Error creating day's wod msg");
                        }
                        Database.BackupDatabase(Constants.DATABASE_BACKUP_PATH);

                        MotivateMsgs.ResetUsed(); // Reset Messages so we can use new ones
                        SnarkMsgs.ResetUsed();
                    } else if (IsTimeAfterTarget(DateTime.Now, dayEnd, timeWindow) && Database.GetWorkoutDayEndMessage(DateTime.Now) < 1) {
                        // -- Day Summary Message
                        Log.Information("Creating today's summary message");
                        CreateTodaysSummaryMessage();

                    } else {
                        // -- Motivation Message
                        CreateMotivateMessage();
                    }
                } catch (Exception ex) {
                    Log.Error(ex, "Main Loop Error!");
                } finally {
                    BotNotBusy.Set();
                }
            }
        }

        private Task LogAsync(LogMessage log) {
            if (log.Exception?.Message == "Server requested a reconnect") {
                Log.Information("Server request a reconnect");

            } else if (log.Exception is HttpRequestException || log.Exception?.Message?.Contains("connection was closed") == true) {
                Log.Information("Server Disco Event! Resuming in 30s...");
                return Task.Delay(TimeSpan.FromSeconds(30));

            } else {
                // Log.Information($"Log: type: {log.Exception?.GetType()}, ex.Msg: {log.Exception?.Message}, Msg: {log.Message}");
                if (log.Exception != null) {
                    Log.Error(log.Exception, log.Message);
                } else {
                    Log.Information(log.Exception, log.Message);
                }

            }

            return Task.CompletedTask;
        }
        private Task ReadyAsync() {
            Log.Information($"{_client.CurrentUser} is connected!");
            Connected.Set();
            BotNotBusy.Set();
            return Task.CompletedTask;
        }

        private void Startup() {
            Log.Debug("Verifiying Permissions emojis...");
            VerifyPermissions();
            Database.BackupDatabase(Constants.DATABASE_BACKUP_PATH);
            Log.Debug("Startup Successful!");
        }
        private async void VerifyPermissions() {
            // TODO this is just fucking terribly ineffecient

            var users = _client.GetGuild(Constants.GuildID).Users.Where(x => !x.IsBot).Select(x => x.Id);
            var chan = GetGoalsChannel();

            var mP = (await GetUsersWhoReactedToMessaged(await chan.GetMessageAsync(Constants.MessageMorningWorkout))).Select(x => x.Id);
            var lP = (await GetUsersWhoReactedToMessaged(await chan.GetMessageAsync(Constants.MessageLunchWorkout))).Select(x => x.Id);
            var eP = (await GetUsersWhoReactedToMessaged(await chan.GetMessageAsync(Constants.MessageAfternoonWorkout))).Select(x => x.Id);

            var cM = (await GetUsersWhoReactedToMessaged(await chan.GetMessageAsync(Constants.MessageMotivateMeOnMissedWorkout))).Select(x => x.Id);
            var nM = (await GetUsersWhoReactedToMessaged(await chan.GetMessageAsync(Constants.MessageMotivateMeOnMultipleMissedWorkouts)))
               .Select(x => x.Id);
            var pM = (await GetUsersWhoReactedToMessaged(await chan.GetMessageAsync(Constants.MessageCallOutHittingGoals))).Select(x => x.Id);

            foreach (var u in users) {
                Database.SetUserWorkoutMorningPerson(u, mP.Any(x => x == u));
                Database.SetUserWorkoutLunchPerson(u, lP.Any(x => x == u));
                Database.SetUserWorkoutEveningPerson(u, eP.Any(x => x == u));

                Database.SetUserCanMotivate(u, cM.Any(x => x == u));
                Database.SetUserNegitiveStreak(u, nM.Any(x => x == u));
                Database.SetUserPositiveStreak(u, pM.Any(x => x == u));
            }
        }
        private static bool IsTimeAfterTarget(DateTime now, DateTime target, TimeSpan timeWindow) {
            return now >= target || target - now < timeWindow;
        }

        private async Task MessageReceivedAsync(SocketMessage message) {
            bool IsCommand(string cmd) {
                return message.Content.StartsWith($"!{cmd}", StringComparison.CurrentCultureIgnoreCase)
                    || message.Content.StartsWith($"#{cmd}", StringComparison.CurrentCultureIgnoreCase);
            }

            bool IsWorkoutOrTestingChannel() {
                return message.Channel.Id is Constants.ChannelWorkouts or Constants.ChannelTesting;
            }

            if (!BotNotBusy.Wait(1000)) {
                Log.Error("MessageReceive abort: Bot Busy!");
                return;
            }
            try {
                BotNotBusy.Reset();

                // -- Delete the 'Bot has pinned a message' messages in the workout channel
                if (message.Channel.Id == Constants.ChannelWorkouts
                 && (message as IMessage).Type == MessageType.ChannelPinnedMessage) {
                    await message.DeleteAsync();
                    return;
                }

                if (IsCommand("snark")) {
                    var reply = string.Empty;

                    IMessage msg = message;
                    if (msg.Reference is { MessageId: { IsSpecified: true } }) {
                        var m = await message.Channel.GetMessageAsync(msg.Reference.MessageId.Value);
                        if (m.Author.Id == Constants.BotID) {
                            reply = $"Nice Try, {MentionUtils.MentionUser(msg.Author.Id)}, but I am not snarky!";
                        } else {
                            msg = m;
                        }
                    }
                    if (reply.Length < 1) {
                        // TODO: else if (msg.MentionedUsers.Any(x => x.Id == Constants.BotID)) -- reply to messages that are just snarky, not just at the bot?
                        reply = SnarkMsgs.GetNext().Replace("{user}", MentionUtils.MentionUser(msg.Author.Id));
                    }
                    await message.Channel.SendMessageAsync(reply, messageReference: new MessageReference(msg.Id, msg.Channel.Id));
                    return;
                }

                // The bot should never respond to itself.
                if (message.Author.IsBot) {
                    return;
                }

                // -- Get a new WOD for today
                if (IsCommand("wod") && (IsWorkoutOrTestingChannel() || message.Channel is IPrivateChannel)) {
                    var wod = GetRandomWod();
                    await message.Channel.SendMessageAsync(wod);
                    return;
                }

                // -- Don't Motivate me today --
                if (IsCommand("nottoday") && IsWorkoutOrTestingChannel() && GetUsersWorkingOutOnDay(DateTime.Now).Contains(message.Author.Id)) {
                    Database.AddMotivatedUser(DateTime.Now, message.Author.Id);
                    await message.AddReactionAsync(new Emoji("🥺"));
                    return;
                }

                // -- Store Workouts --
                if (IsCommand("workout") && IsWorkoutOrTestingChannel()) {
                    await StoreTodaysWorkoutMessage(message);
                    return;
                }
                if (IsCommand("yesterday") && IsWorkoutOrTestingChannel() && message.Timestamp.LocalDateTime.Date == DateTime.Today.AddDays(-1)) {
                    await StoreYesterdaysWorkoutMessage(message);
                    return;
                }

                // -- Recal User History --
                if (IsCommand("log")) {
                    if (message.Channel is not IPrivateChannel) {
                        await message.DeleteAsync();
                    }
                    RequestUserWorkoutLog(message);
                    return;
                }

                // -- Testing Functions
                if (message.Channel.Id == Constants.ChannelTesting) {
                    if (message.Content == "!backup") {
                        Database.BackupDatabase(Constants.DATABASE_BACKUP_PATH, true);
                        return;
                    }
                    if (message.Content == "!newUserJoinTest") {
                        await HandleUserJoined(message.Author);
                        return;
                    }
                    if (message.Content.StartsWith("!say ")) {
                        var msg = message.Content[5..];
                        await GetDiscussionChannel().SendMessageAsync(msg);
                        return;
                    }
                    if (message.Content == "!dayStart") {
                        Log.Debug("Manually Creating today's summary message");
                        CreateTodaysWorkoutMessage();
                        return;
                    }
                    if (message.Content == "!dayStartMsg") {
                        var msg = CompileDayStartMessage(DateTime.Now, null, true);
                        await GetTestingChannel().SendMessageAsync(msg);
                        return;
                    }
                    if (message.Content == "!dayEnd") {
                        Log.Debug("Manually Creating today's summary message");
                        CreateTodaysSummaryMessage();
                        return;
                    }
                    if (message.Content == "!dayEndMsg") {
                        var msg = CompileDaySummaryMessage(DateTime.Now, true);
                        await GetTestingChannel().SendMessageAsync(msg);
                        return;
                    }

                    if (message.Content == "!motivate") {
                        CreateMotivateMessage();
                        return;
                    }

                    if (message.Content.StartsWith("!delete ")) {
                        var split = message.Content.Split(' ');
                        if (split.Length < 2 || !ulong.TryParse(split[1], out var mid)) {
                            return;
                        }

                        await GetWorkoutsChannel().DeleteMessageAsync(mid);
                        Log.Debug("Deleted message {mid}", mid);
                        return;
                    }

                    if (message.Content.StartsWith("!store ")) {
                        var split = message.Content.Split(' ', StringSplitOptions.RemoveEmptyEntries);
                        if (split.Length > 1 && ulong.TryParse(split[1], out var mid)) {
                            var msg = await GetWorkoutsChannel().GetMessageAsync(mid);
                            if (msg == null) {
                                Log.Debug("Cannot Store msg, msg not found");
                                return;
                            }
                            await StoreTodaysWorkoutMessage(msg);
                        }
                        return;
                    }

                    if (message.Content.StartsWith("!storeMissed ")) {
                        var split = message.Content.Split(' ', StringSplitOptions.RemoveEmptyEntries);
                        if (split.Length > 2 && ulong.TryParse(split[1], out var uid) && DateTime.TryParse(split[2], out var dt)) {
                            Database.StoreMissedWorkout(dt, uid);
                        }
                        return;
                    }

                    if (message.Content == "!test") {
                        var s = GetWorkoutStreaks(DateTime.Now, 14);
                        Log.Debug("Streaks          : {m}",
                                  string.Join(", ", GetWorkoutStreaks(DateTime.Now, 14).Select(x => $"{_client.GetUser(x.Key).Username}\t=> {x.Value}")));

                        Log.Debug("-- Streak Test ---------------------------------------------------------------------");
                        var data = Database.GetWorkoutOverview(DateTime.Now.AddDays(-7), DateTime.Now.AddDays(1));
                        if (data.Count < 1) {
                            Log.Debug("No Entries in returned Workout Log");
                            return;
                        }

                        Log.Debug("Workout Log Overview:");
                        foreach (var uid in data.Keys) {
                            var user = _client.GetUser(uid).Username;
                            Log.Debug("-- User: {username}", user);

                            var streak = 0;
                            foreach (var (date, missed, extra) in data[uid].OrderBy(x => x.Date)) {
                                if (!missed) {
                                    streak++;
                                } else {
                                    streak = 0;
                                }
                                Log.Debug($"\t[{date:yyyy-MM-dd}] {(missed ? "Missed" : extra ? "Extra" : "Worked Out")} (streak: {streak})");
                            }
                        }

                        Log.Debug("-- Can Motivate Test ---------------------------------------------------------------------");
                        var day = new DateTime(2021, 01, 27);
                        Log.Debug("Working Out          : {m}", string.Join(", ", GetUsersWorkingOutOnDay(day).Select(x => _client.GetUser(x).Username)));
                        Log.Debug("Can Motivate         : {m}", string.Join(", ", Database.GetUsersWhoAllowMotivate().Select(x => _client.GetUser(x).Username)));
                        Log.Debug("Who's Worked Out     : {m}",
                                  string.Join(", ", Database.GetUsersWhoHaveWorkoutedOut(day).Select(x => _client.GetUser(x).Username)));
                        Log.Debug("Who's Behind @ 0900  : {m}", string.Join(", ", GetWhosBehind(day.AddHours(09)).Select(x => _client.GetUser(x).Username)));
                        Log.Debug("Who's Behind @ 1300  : {m}", string.Join(", ", GetWhosBehind(day.AddHours(13)).Select(x => _client.GetUser(x).Username)));
                        Log.Debug("Who's Behind @ 1700  : {m}", string.Join(", ", GetWhosBehind(day.AddHours(17)).Select(x => _client.GetUser(x).Username)));
                        Log.Debug("Who's Behind @ 2100  : {m}", string.Join(", ", GetWhosBehind(day.AddHours(21)).Select(x => _client.GetUser(x).Username)));
                    }
                }
            } finally {
                BotNotBusy.Set();
            }
        }
        private Task MessageRemovedAsync(Cacheable<IMessage, ulong> cachedMsg, ISocketMessageChannel originChannel) {
            if (!cachedMsg.HasValue || !BotNotBusy.Wait(1000)) {
                Log.Error("Message Removed abort: Bot Busy!");
                return Task.CompletedTask;
            }
            try {
                BotNotBusy.Reset();
                var timeOkayToChangeMessage = DateTime.Today - cachedMsg.Value.Timestamp.LocalDateTime.Date <= TimeSpan.FromDays(2);

                if (originChannel.Id == Constants.ChannelWorkouts && timeOkayToChangeMessage) {
                    if (cachedMsg.HasValue && !cachedMsg.Value.Content.StartsWith("#Workout", StringComparison.CurrentCultureIgnoreCase)
                     && !cachedMsg.Value.Content.StartsWith("!Workout", StringComparison.CurrentCultureIgnoreCase)) {
                        return Task.CompletedTask;
                    }

                    if (Database.RemoveTaggedWorkout(cachedMsg.Id)) {
                        Log.Debug("Removed Tagged Workout: {msg}", cachedMsg.Id);
                    } else {
                        Log.Debug("Asked to remove Tagged Workout ({msg}) but didn't have it stored", cachedMsg.Id);
                    }
                }
                return Task.CompletedTask;
            } finally {
                BotNotBusy.Set();
            }
        }
        private async Task MessageEditedAsync(Cacheable<IMessage, ulong> cachedMsg, SocketMessage message, ISocketMessageChannel originChannel) {
            if (message.Author.Id == _client.CurrentUser.Id) {
                return;
            }

            // Only react to message edited created today
            if (message.Timestamp.LocalDateTime.Date != DateTime.Now.Date) {
                return;
            }

            if (!BotNotBusy.Wait(1000)) {
                Log.Error("Message Edited abort: Bot Busy!");
                return;
            }
            try {
                BotNotBusy.Reset();

                // Message Storage Test
                if ((message.Channel.Id == Constants.ChannelWorkouts || message.Channel.Id == Constants.ChannelTesting)
                 && (message.Content.StartsWith("!Workout", StringComparison.CurrentCultureIgnoreCase)
                  || message.Content.StartsWith("#Workout", StringComparison.CurrentCultureIgnoreCase))) {
                    Log.Debug("Updating Tagged Workout: {msg}", message);
                    Database.StoreTaggedWorkout(message, IsWorkoutExtraForUser(message.Timestamp.LocalDateTime, message.Author.Id));

                    // React to this message if we have not before
                    var extra = IsWorkoutExtraForUser(message.Timestamp.LocalDateTime, message.Author.Id);
                    var emoji = new Emoji(extra ? "🏅" : "🏋");
                    if (!message.Reactions.ContainsKey(emoji) || message.Reactions[emoji].IsMe == false) {
                        await message.AddReactionAsync(emoji);
                    }
                }
            } finally {
                BotNotBusy.Set();
            }
        }
        private async Task HandleReactionAddedAsync(Cacheable<IUserMessage, ulong> cachedMsg, ISocketMessageChannel originChannel,
                                                    SocketReaction reaction) {
            if (!reaction.User.IsSpecified || reaction.User.Value.IsBot) {
                return;
            }

            if (!BotNotBusy.Wait(1000)) {
                Log.Error("Msg Reaction Added abort: Bot Busy!");
                return;
            }
            try {
                BotNotBusy.Reset();
                var uid = reaction.UserId;
                if (originChannel.Id == Constants.ChannelWorkouts) {
                    var dayStartMsg = Database.GetWorkoutDayStartMessage(DateTime.Now);
                    if (dayStartMsg == cachedMsg.Id) {
                        var timeOkayToStoreMessage = Database.GetWorkoutDayEndMessage(DateTime.Now) < 1;
                        if (timeOkayToStoreMessage) {
                            var extra = IsWorkoutExtraForUser(DateTime.Now, uid);
                            Log.Debug("Storing Emoted Workout for {user} (extra: {e})", reaction.User.GetValueOrDefault().Username, extra);
                            Database.StoreEmotedWorkout(DateTime.Now, uid, extra);
                        } else {
                            Log.Debug("Cannot Store Emoted Workout for {user} since it is between dayStart and dayEnd",
                                      reaction.User.GetValueOrDefault()?.Username);
                        }
                    }
                }

                if (originChannel.Id == Constants.ChannelGoals) {
                    // Options / Permissions
                    if (cachedMsg.Id == Constants.MessageMorningWorkout) {
                        Database.SetUserWorkoutMorningPerson(uid, true);
                    } else if (cachedMsg.Id == Constants.MessageLunchWorkout) {
                        Database.SetUserWorkoutLunchPerson(uid, true);
                    } else if (cachedMsg.Id == Constants.MessageAfternoonWorkout) {
                        Database.SetUserWorkoutEveningPerson(uid, true);
                    } else if (cachedMsg.Id == Constants.MessageMotivateMeOnMissedWorkout) {
                        Database.SetUserCanMotivate(uid, true);
                    } else if (cachedMsg.Id == Constants.MessageMotivateMeOnMultipleMissedWorkouts) {
                        Database.SetUserNegitiveStreak(uid, true);
                    } else if (cachedMsg.Id == Constants.MessageCallOutHittingGoals) {
                        Database.SetUserPositiveStreak(uid, true);
                    } else {
                        // Workout Day
                        var role = Utility.GetDaysWorkoutRoleName(cachedMsg.Id);
                        if (role == 0) {
                            Log.Error("User Reacted on role message with unknown ID: {id}", cachedMsg.Id);
                            return;
                        }

                        var userRole = _client.GetGuild(Constants.GuildID).GetRole(role);
                        if (userRole == null) {
                            Log.Error("User Reacted on role message related to an unknown role: Message: {mid}, returned role: {rid}", cachedMsg.Id, role);
                            return;
                        }

                        await _client.GetGuild(Constants.GuildID).GetUser(uid).AddRoleAsync(userRole);
                        Log.Information($"Added role '{userRole.Name}' to user {_client.GetGuild(Constants.GuildID).GetUser(reaction.UserId).Username}!");
                    }
                }
            } finally {
                BotNotBusy.Set();
            }
        }
        private async Task HandleReactionRemovedAsync(Cacheable<IUserMessage, ulong> cachedMsg, ISocketMessageChannel originChannel,
                                                      SocketReaction reaction) {
            if (!reaction.User.IsSpecified || reaction.User.Value.IsBot) {
                return;
            }
            var uid = reaction.UserId;

            if (!BotNotBusy.Wait(1000)) {
                Log.Error("Msg Reaction Removed abort: Bot Busy!");
                return;
            }
            try {
                BotNotBusy.Reset();
                if (originChannel.Id == Constants.ChannelWorkouts) {
                    var timeOkayToChangeMessage = Database.GetWorkoutDayEndMessage(DateTime.Now) < 1;
                    if (timeOkayToChangeMessage) {
                        Log.Debug("Removing Emoted Workout for {user}", reaction.User.GetValueOrDefault().Username);
                        var dayStartMsg = Database.GetWorkoutDayStartMessage(DateTime.Now);
                        if (dayStartMsg == cachedMsg.Id) {
                            Database.RemoveEmotedWorkout(DateTime.Now, uid);
                        }
                    } else {
                        Log.Debug("Cannot Removed Emoted Workout for {user} since it is between dayStart and dayEnd",
                                  reaction.User.GetValueOrDefault()?.Username);
                    }
                }

                if (originChannel.Id == Constants.ChannelGoals) {
                    // Options / Permissions
                    if (cachedMsg.Id == Constants.MessageMorningWorkout) {
                        Database.SetUserWorkoutMorningPerson(uid, false);
                    } else if (cachedMsg.Id == Constants.MessageLunchWorkout) {
                        Database.SetUserWorkoutLunchPerson(uid, false);
                    } else if (cachedMsg.Id == Constants.MessageAfternoonWorkout) {
                        Database.SetUserWorkoutEveningPerson(uid, false);
                    } else if (cachedMsg.Id == Constants.MessageMotivateMeOnMissedWorkout) {
                        Database.SetUserCanMotivate(uid, false);
                    } else if (cachedMsg.Id == Constants.MessageMotivateMeOnMultipleMissedWorkouts) {
                        Database.SetUserNegitiveStreak(uid, false);
                    } else if (cachedMsg.Id == Constants.MessageCallOutHittingGoals) {
                        Database.SetUserPositiveStreak(uid, false);
                    } else {
                        // Workout Day
                        var role = Utility.GetDaysWorkoutRoleName(cachedMsg.Id);
                        if (role == 0) {
                            Log.Error("User Reacted on role message with unknown ID: {id}", cachedMsg.Id);
                            return;
                        }

                        var userRole = _client.GetGuild(Constants.GuildID).GetRole(role);
                        if (userRole == null) {
                            Log.Error("User Reacted on role message related to an unknown role: Message: {mid}, returned role: {rid}", cachedMsg.Id, role);
                            return;
                        }

                        await _client.GetGuild(Constants.GuildID).GetUser(uid).RemoveRoleAsync(userRole);
                        Log.Information($"Removed role '{userRole.Name}' from user {_client.GetGuild(Constants.GuildID).GetUser(reaction.UserId).Username}!");
                    }
                }
            } finally {
                BotNotBusy.Set();
            }
        }
        private async Task StoreTodaysWorkoutMessage(IMessage message) {
            Debug.Assert(message.Timestamp.LocalDateTime.Date == DateTime.Today);
            var extra = IsWorkoutExtraForUser(message.Timestamp.LocalDateTime, message.Author.Id);

            Log.Debug("Storing Today's Workout: {MessageText} (extra: {IsExtra}) for {Username}", message, extra, message.Author.Username);
            Database.StoreTaggedWorkout(message, extra);

            // Communicate to user that workout was stored
            var emoji = extra ? "🏅" : "🏋";
            await message.AddReactionAsync(new Emoji(emoji));
        }
        private async Task StoreYesterdaysWorkoutMessage(IMessage message) {
            Debug.Assert(message.Timestamp.LocalDateTime.Date == DateTime.Today.AddDays(-1));
            var extra = IsWorkoutExtraForUser(message.Timestamp.LocalDateTime, message.Author.Id);

            Log.Debug("Storing Yesterday's Workout: {MessageText} (extra: {IsExtra}) for {Username}", message, extra, message.Author.Username);
            Database.StoreTaggedWorkout(message, extra);

            // Communicate to user that workout was stored
            await message.AddReactionAsync(new Emoji("📅"));
            await message.AddReactionAsync(new Emoji(extra ? "🏅" : "🏋"));
        }
        private async Task HandleUserJoined(IUser user) {
            await user.SendMessageAsync(
                $"Hello and welcome to the Fitness Club, {MentionUtils.MentionUser(user.Id)}!! Please head over to {MentionUtils.MentionChannel(Constants.ChannelGoals)} and start at the top to learn a bit about how this chat works! Good luck and have fun!");
        }

        #region -- WODS --
        private string GetRandomWod() {
            try {
                var wods = File.ReadAllText(Constants.WOD_PATH).Split(',');
                if (wods.Length < 1) {
                    return "error";
                }

                var wod = wods[Random.Next(0, wods.Length)];

                // Chomp the leading/trailing "
                wod = wod[1..^1];

                var i = wod.IndexOf("<br><br>");
                if (i > 0) {
                    var title = $"**{wod[..i]}--**";
                    var workout = wod[(i + 8)..].Replace("<br>", "\n\t");

                    return $"**{title}**\n\t{workout}";
                }
                return wod;
            } catch (Exception ex) {
                Log.Debug(ex, "Error during GetRandomWod:");
                return "Error";
            }
        }
        #endregion

        #region -- Checkin Messages --
        private async void CreateTodaysWorkoutMessage() {
            var users = GetUsersWorkingOutOnDay(DateTime.Now);
            var dayStartString = CompileDayStartMessage(DateTime.Now, users);
            var msg = await GetWorkoutsChannel().SendMessageAsync(dayStartString);
            Log.Debug("Create Day Start Msg, got id {id}", msg);
            Database.SetWorkoutDayStartMessage(DateTime.Now, msg.Id, users);
            Log.Debug("Storing Day Start Msg in DB with, Date: {Date:yyyy-MM-dd}, MsgId: {MsgId}, Users: {Username}", DateTime.Now, msg,
                      string.Join(", ", users.Select(x => _client.GetUser(x).Username)));

            // Unpin previous messages
            var toUnpin = await GetWorkoutsChannel().GetPinnedMessagesAsync();
            foreach (var m in toUnpin) {
                if (m is not IUserMessage um) {
                    continue;
                }

                try {
                    await um.UnpinAsync();
                } catch (Exception ex) {
                    Log.Debug(ex, "Error trying to unpin msg {MessageTime} ({MessageID})", um.Timestamp.ToLocalTime(), um.Id);
                }
            }

            // Pin today's msg
            try {
                // await Task.Delay(500);
                // var m = (IUserMessage)await GetWorkoutsChannel().GetMessageAsync(msg.Id);
                // await m.PinAsync();
                await msg.PinAsync();
                Log.Debug("...And pinned it successfully");
            } catch (Exception e) {
                Log.Error(e, "Got error trying to pin today's Start Message");
            }
        }
        private string CompileDayStartMessage(DateTime date, IEnumerable<ulong> users = null, bool test = false) {
            if (users == null) {
                users = GetUsersWorkingOutOnDay(date);
            }

            if (!users.Any()) {
                return "No one is set to workout today? How lame!";
            }

            var workoutEntries = Database.GetUsersWhoHaveWorkoutedOut(date);

            var mentions = users.Select(x => test ? _client.GetUser(x).Username : MentionUtils.MentionUser(x));
            var alreadyWorked = workoutEntries.Select(x => test ? _client.GetUser(x).Username : MentionUtils.MentionUser(x));
            return $"Good Morning 🌅, it's {DateTime.Now.DayOfWeek}, the {DateTime.Now.Day}{Utility.GetDaySuffix(DateTime.Now.Day)}!\n"
                 + MorningPrefixMsgs.GetNext() + '\n'
                 + $"Today's workout-ers: {Utility.JoinHumanString(mentions)}\n"
                 + (workoutEntries.Count > 0
                        ? $"Holy cow, {Utility.JoinHumanString(alreadyWorked)} already got it done! Way to go early riser{(alreadyWorked.Count() > 1 ? "s" : "")}!\n"
                        : "")
                 + MorningSuffixMsgs.GetNext() + " 🌞 " + Constants.DayWorkoutMsgKey;

        }
        private async void CreateMotivateMessage(int hour = -1) {
            var time = hour < 0 ? DateTime.Now : DateTime.Now.Date.AddHours(hour);

            var behind = GetWhosBehind(time);
            var permission = Database.GetUsersWhoAllowMotivate();

            var canMotivate = behind.Intersect(permission);
            if (!canMotivate.Any()) {
                return;
            }

            var idsOfUsersMotivatedAlready = Database.GetMotivatedUsers(time);
            var leftToMotivate = canMotivate.Where(x => !idsOfUsersMotivatedAlready.Contains(x));
            if (!leftToMotivate.Any()) {
                return;
            }

            var toMotivate = Random.Next(leftToMotivate.Count() - 1);
            var userID = leftToMotivate.ElementAt(toMotivate);
            await GetWorkoutsChannel().SendMessageAsync(
                $"{Constants.DayMotivateMsgKey} {MotivateMsgs.GetNext().Replace("{user}", MentionUtils.MentionUser(userID))}"
            );
            Database.AddMotivatedUser(time, userID);
        }
        private async void CreateTodaysSummaryMessage() {
            var workoutChannel = GetWorkoutsChannel();
            using var inTypingState = workoutChannel.EnterTypingState();

            var endMsg = await workoutChannel.SendMessageAsync(CompileDaySummaryMessage(DateTime.Now));
            Database.SetWorkoutDaySummaryMessage(DateTime.Now, endMsg.Id);
            try {
                await Task.Delay(500);
                var m = (IUserMessage)await workoutChannel.GetMessageAsync(endMsg.Id);
                if (m == null) {
                    Log.Debug("Message was null!");
                } else {
                    await m.PinAsync();
                }
                Log.Debug("...And pinned it successfully");
            } catch (Exception e) {
                Log.Error(e, "Got error trying to pin today's Summary Message");
            }
        }
        private string CompileDaySummaryMessage(DateTime date, bool test = false) {
            Log.Debug("Starting Compile Today Msg...");

            // Get Permissions for the types of Messages we can send
            var permNegativeStreaks = Database.GetUsersWhoAllowNegitiveStreaks();
            var permPositiveStreaks = Database.GetUsersWhoAllowPositiveStreaks();

            var users = GetUsersWorkingOutOnDay(date).Select(x => x);
            var usersDayStart = Database.GetDayStartWorkingOutUsers(date);
            var workoutEntries = Database.GetUsersWhoHaveWorkoutedOut(date);
            var mustWorkoutToday = usersDayStart.Intersect(users);
            var usersWhoMissed = mustWorkoutToday.Except(workoutEntries);

            if (!mustWorkoutToday.Any() && !workoutEntries.Any()) {
                return
                    $"{Constants.DaySummaryMsgKey} No one failed if no one's scheduled! I guess everyone needs a day off sometime...\n*I'm just a little lonely...* 😭";
            }

            var streaks = GetWorkoutStreaks(date);
            Log.Debug("Users:\t{m}", string.Join(", ", users.Select(x => _client.GetUser(x).Username)));
            Log.Debug("Users Start:\t{m}", string.Join(", ", usersDayStart.Select(x => _client.GetUser(x).Username)));
            Log.Debug("MustWorkout:\t{m}", string.Join(", ", mustWorkoutToday.Select(x => _client.GetUser(x).Username)));
            Log.Debug("Workouts:\t{m}", string.Join(", ", workoutEntries.Select(x => _client.GetUser(x).Username)));
            Log.Debug("Missed:\t{m}", string.Join(", ", usersWhoMissed.Select(x => _client.GetUser(x).Username)));
            Log.Debug("Streaks:\t{m}", string.Join(", ", streaks.Select(x => $"{_client.GetUser(x.Key).Username}\t=> {x.Value}")));

            var msg = new StringBuilder();
            msg.Append(Constants.DaySummaryMsgKey);
            msg.AppendLine(" Time's Up, the day is over! Let's see how everyone did today...");

            // Store missed workouts for users who did not complete
            if (!test) {
                foreach (var u in usersWhoMissed) {
                    Database.StoreMissedWorkout(DateTime.Now, u);
                }
            }

            // -- Build User -> Day's Workout Status list
            foreach (var user in users.Union(usersDayStart).Union(workoutEntries)) {
                var userStr = test ? _client.GetUser(user).Username : MentionUtils.MentionUser(user);

                var workedOut = workoutEntries.Contains(user);
                var missed = usersWhoMissed.Contains(user);
                var extra = !mustWorkoutToday.Contains(user) && workedOut;
                var changedPlanTo = !usersDayStart.Contains(user) && users.Contains(user);
                var changedPlanAway = usersDayStart.Contains(user) && !users.Contains(user);

                var streak = streaks.ContainsKey(user) ? streaks[user] : workedOut ? 1 : 0;
                var canMentionStreak = streak > 0 && permPositiveStreaks.Contains(user) || streak < 0 && permNegativeStreaks.Contains(user);

                if (changedPlanTo && workedOut) {
                    msg.AppendLine($"{userStr} changed their plan to today and worked out! That's extra credit in my book 🏅");
                } else if (changedPlanTo) {
                    msg.AppendLine($"{userStr} changed their plan to today.");
                } else if (changedPlanAway && workedOut) {
                    msg.AppendLine($"{userStr} changed their plan away from today but still worked out, I'll count that as Extra!");
                } else if (changedPlanAway) {
                    msg.AppendLine($"{userStr} changed their plan away from today.");
                } else if (missed) {
                    msg.AppendLine($"{userStr} missed their workout today 😿.");
                } else if (extra) {
                    msg.AppendLine($"{userStr} put in some __*extra*__ work today, wow: (•_•) ( •_•)>⌐■-■ (⌐■_■)");
                } else if (Math.Abs(streak) > 3 && canMentionStreak) {
                    msg.AppendLine(
                        (streak > 0 ? StreakPosMsgs.GetNext() : StreakNegMsgs.GetNext())
                       .Replace("{user}", userStr)
                       .Replace("{streak}", Math.Abs(streaks[user]).ToString())
                    );
                } else {
                    msg.AppendLine($"{userStr} got it done.");
                }
            }

            // -- Finish up the msg with a nice summary of how the group did as a whole
            if (mustWorkoutToday.Count() > 2) {
                msg.AppendLine(GenerateDaySummaryWrapUpMessage(
                                   !usersWhoMissed.Any(),
                                   usersWhoMissed.Count() == mustWorkoutToday.Count())
                );
            }

            return msg.ToString();
        }
        #endregion

        #region -- Get Workout History --
        private async void RequestUserWorkoutLog(IMessage msg) {
            if (msg == null) {
                return;
            }

            var start = DateTime.Now.Date;
            var end = DateTime.Now.Date.AddDays(1);

            var options = msg.Content.Split(' ').ToList();
            if (options.Count > 1 && options[1] == "last") {
                options.RemoveAt(1);
            }

            if (options.Count > 2 && uint.TryParse(options[1], out var num)) {
                var daysBack = options[2] switch {
                    "week" => -num * 7,
                    "weeks" => -num * 7,
                    "month" => -num * 31,
                    "months" => -num * 31,
                    _ => -num // default is days
                };
                start = start.AddDays(daysBack);
                Log.Debug("RequestUserWorkoutLog, got request {req}, parsed as {0} days back", msg.Content, -num);
            } else if (options.Count > 2) {
                if (!DateTime.TryParse(options[1], out var d1)) {
                    await _client.GetUser(msg.Author.Id)
                                 .SendMessageAsync(
                                      $"Sorry, could not parse this Date Time string: '{options[1]}', try the format 'year-MM-dd' like a sane person!");
                    return;
                }
                if (!DateTime.TryParse(options[2], out var d2)) {
                    await _client.GetUser(msg.Author.Id)
                                 .SendMessageAsync(
                                      $"Sorry, could not parse this Date Time string: '{options[2]}', try the format 'year-MM-dd' like a sane person!");
                    return;
                }
                if (d1 < d2) {
                    start = d1;
                    end = d2;
                } else {
                    start = d2;
                    end = d1;
                }
                Log.Debug("RequestUserWorkoutLog, got request {req}, parsed as {start:yyyyMMdd} to {end:yyyyMMdd}", msg.Content, start, end);
            } else if (options.Count > 1) {
                if (!DateTime.TryParse(options[1], out var d1)) {
                    await _client.GetUser(msg.Author.Id)
                                 .SendMessageAsync(
                                      $"Sorry, could not parse this Date Time string: '{options[1]}', try the format 'year-MM-dd' like a sane person!");
                    return;
                }
                start = d1;
                Log.Debug("RequestUserWorkoutLog, got request {req}, parsed as from {start:yyyyMMdd}", msg.Content, start);
            } else {
                start = DateTime.MinValue;
                end = DateTime.MaxValue;
            }
            SendUserWorkoutLog(msg.Author.Id, start, end, true);
        }
        private async void SendUserWorkoutLog(ulong uid, DateTime start, DateTime end, bool dm = false) {
            var entries = Database.GetUserWorkouts(uid, start, end);
            var msgs = new List<string>();

            if (entries.Count < 1) {
                msgs.Add("No entries found");
            } else {
                var sb = new StringBuilder();
                foreach (var e in entries) {
                    var high = e.Missed ? "-" : e.Extra ? "+" : "";
                    var str = e.Description.Length < 1 && e.Media.Length < 1 ?
                                  $"```diff\n{high} [{e.Date:yyyy-MM-dd}] => {(e.Missed ? "Missed" : e.Extra ? "Extra!" : "Completed")}\n```"
                                  : $"```diff\n{high} [{e.Date:yyyy-MM-dd}] => {e.Description.Replace("```", "").Replace("\n", $"\n{high}\t\t")}\n```{(e.Media.Length > 0 ? $"\n{e.Media.Replace(",", " ")}" : "")}";

                    if (sb.Length + str.Length > 2000) {
                        msgs.Add($"\n{sb}\n");
                        sb.Clear();
                    }
                    sb.AppendLine(str);
                }
                if (sb.Length > 0) {
                    msgs.Add($"\n{sb}\n");
                }
            }

            if (dm) {
                var send = _client.GetUser(uid);
                foreach (var m in msgs) {
                    await send.SendMessageAsync($"{m}");
                }
            } else {
                var send = GetDiscussionChannel();
                foreach (var m in msgs) {
                    await send.SendMessageAsync($"```diff\n{m}\n```");
                }
            }
        }
        private Dictionary<ulong, int> GetWorkoutStreaks(DateTime start, int daysBack = 365) {
            var hist = Database.GetWorkoutOverview(start.AddDays(-daysBack), start.AddDays(1));
            var streaks = new Dictionary<ulong, int>(hist.Count);

            foreach (var e in hist) {
                var user = _client.GetUser(e.Key);
                if (user == null) {
                    continue;
                }

                Log.Debug("GetStreak: Starting {user}...", _client.GetUser(e.Key).Username);
                if (e.Value.Count < 1) {
                    continue;
                }

                if (e.Value[0].extra) {
                    continue; // todo how to do this
                }

                // var extras = e.Value.Sum(x => x.extra ? 1 : 0);
                var extrasUsed = new List<DateTime>();
                var streak = e.Value[0].missed ? -1 : 1;
                for (var i = 1; i < e.Value.Count; i++) {
                    var (date, missed, extra) = e.Value[i];
                    if (extra) {
                        continue;
                    }

                    // exit on streak end (e.x., Was working then this is a miss, or was missing but this is a workout)
                    if (streak < 1 && !missed) {
                        break;
                    }
                    if (streak > 0 && missed) {
                        // See if the have any extras to cover this missed
                        var extraAvailStart = date.AddDays(-7);
                        var extraAvailEnd = date.AddDays(7);
                        var hasExtra = e.Value.Where(x => x.extra && x.Date >= extraAvailStart && x.Date <= extraAvailEnd && !extrasUsed.Contains(x.Date))
                                        .Select(x => x.Date);
                        if (!hasExtra.Any()) {
                            break;
                        }

                        // Deduct the cost
                        // extras--;
                        extrasUsed.Add(hasExtra.First());
                        missed = false;
                    }
                    streak += missed ? -1 : 1;
                    Log.Debug("GetStreak: {user}\t{streak}\t (entry: {missed} @ {date:yyyy-MM-dd}, extras: {extras})", _client.GetUser(e.Key).Username, streak,
                              missed, date, extrasUsed.Count);
                }

                streaks[e.Key] = streak;
            }

            return streaks;
        }
        #endregion

        #region -- Missed Workout --
        private bool IsWorkoutExtraForUser(DateTime when, ulong uid) {
            return !GetUsersWorkingOutOnDay(when).Contains(uid);
        }
        private IEnumerable<ulong> GetUsersWorkingOutOnDay(DateTime when) {
            return _client
                  .GetGuild(Constants.GuildID)
                  .Roles
                  .First(x => x.Id == Utility.GetDaysWorkoutRoleName(when.DayOfWeek))
                  .Members
                  .Select(x => x.Id);
        }
        private IEnumerable<ulong> GetUsersWhoHaveNotWorkedOutOnDate(DateTime when) {
            var usersWhoWorkedOut = Database.GetUsersWhoHaveWorkoutedOut(when);
            return GetUsersWorkingOutOnDay(when).Where(a => !usersWhoWorkedOut.Contains(a));
        }
        private IEnumerable<ulong> GetWhosBehind(DateTime date) {
            var usersWhoShouldHaveWorkoutByNow = new List<ulong>();

            // Don't send any messages after summary or before day start message
            if (date.Hour < Constants.DayStartMessageHour || date.Hour >= Constants.DaySummaryHour) {
                return new List<ulong>();
            }

            if (date.Hour >= Constants.DayMorningWorkoutEndHour) {
                usersWhoShouldHaveWorkoutByNow.AddRange(Database.GetUsersWhoWorkoutInMorning());
            }
            if (date.Hour >= Constants.DayLunchWorkoutEndHour) {
                usersWhoShouldHaveWorkoutByNow.AddRange(Database.GetUsersWhoWorkoutAtLunch());
            }
            if (date.Hour >= Constants.DayAfternoonWorkoutEndHour) {
                usersWhoShouldHaveWorkoutByNow.AddRange(Database.GetUsersWhoWorkoutInEvening());
            }

            var workingOutToday = GetUsersWhoHaveNotWorkedOutOnDate(date);
            return workingOutToday.Where(x => usersWhoShouldHaveWorkoutByNow.Contains(x));
        }
        #endregion

        #region -- Find Bot Messages --
        private async Task<IMessage> FindWorkoutMessageForDay(DateTime date) {
            var msg = Database.GetWorkoutDayStartMessage(date);
            if (msg < 1) {
                return null;
            }
            return await GetWorkoutsChannel().GetMessageAsync(msg);
        }
        private async Task<IMessage> FindSummaryMessageForDay(DateTime date) {
            var msg = Database.GetWorkoutDayEndMessage(date);
            if (msg < 1) {
                return null;
            }
            return await GetWorkoutsChannel().GetMessageAsync(msg);
        }
        #endregion

        #region -- Utility --
        private static async Task<IEnumerable<IUser>> GetUsersWhoReactedToMessaged(IMessage message) {
            var users = new List<IUser>();
            if (message == null) {
                return users;
            }

            foreach (var emoji in message.Reactions) {
                var reactedUsers = await message.GetReactionUsersAsync(emoji.Key, 100).FlattenAsync();
                users.AddRange(reactedUsers);
            }
            return users.Where(x => !x.IsBot);
        }
        private SocketTextChannel GetDiscussionChannel() {
            return _client.GetGuild(Constants.GuildID).GetTextChannel(Constants.ChannelDiscussion);
        }
        private SocketTextChannel GetGoalsChannel() {
            return _client.GetGuild(Constants.GuildID).GetTextChannel(Constants.ChannelGoals);
        }
        private SocketTextChannel GetWorkoutsChannel() {
            return _client.GetGuild(Constants.GuildID).GetTextChannel(Constants.ChannelWorkouts);
        }
        private SocketTextChannel GetTestingChannel() {
            return _client.GetGuild(Constants.GuildID).GetTextChannel(Constants.ChannelTesting);
        }
        #endregion

        #region --------------- Messages ---------------
        private static IEnumerable<string> GenerateGoodMorningPrefix() {
            return new[] {
          "Grab your coffeeeeeeeeee and lets get ready for today!!",
          "Get yourself some java and get your groove on!",
          "Get yourself some java and get your groove on!"
      };
        }
        private static IEnumerable<string> GenerateGoodMorningSuffix() {
            return new[] {
          "*GO GO* **GO** ***GO!!***",
          "*Let's* **do** *this*",
          "*Let's* **do** *this*",
          "Exercise not only changes your body, it changes your mind, your attitude, and your mood!",
          "Do something today that your future self will thank you for!",
          "You shall gain, but you shall pay with sweat, blood, and vomit.",
          "When you have a clear vision of your goal, it’s easier to take the first step toward it.",
          "Remember: **Motivation** is what gets you started, but **Habit** is what keeps you going!",
          "Exercise is a celebration of what your body can do, not a punishment for what you ate!",
          "Work out. Eat well. Be patient -- Your body will reward you!",
          "Personally, I go to the gym because I think my great personality could use a banging body :smirk: :nice: :robot:"
      };
        }
        private static IEnumerable<string> GenerateMotivateMessage() {
            return new[] {
          "Let's go {user}!, there's still time left in the day to make your goals happen!",
          "Hey, {user}! Time to move! GANBATTE!!",
          "Where's my workout, {user}?  https://media.giphy.com/media/T4HPyeDzjyKAg/giphy.gif",
          "{user} https://gph.is/g/Z8pyLNz",
          "Feeling lazy {user}? I think this is for you... https://media.giphy.com/media/xT9DPIBYf0pAviBLzO/giphy.gif",
          "{user} https://media.giphy.com/media/xUNda1JS4IPz91RVPW/giphy.gif",
          "{user} https://media.giphy.com/media/rMS89RxHOzjGw/giphy.gif",
          "Wake up {user}!, do it like Rapinoe and let's gooooooooooo! https://media.giphy.com/media/HrQf6SHCWsPcXJyVwW/giphy.gif",
          "C'mon {user}, you can do it!",
          "Pave the way {user}, put your back into it!",
          "{user}: Tell us why! Show us how! :raised_hands:",
          "Look at where you came from, {user}, look at you now! :clap: :clap:",
          "Do something today that your future self will thank you for, {user}!",
          "You are stronger than you think, {user}!",
          "It may hurt now, but one day it will be your warm-up, {user}!",
          "Let's go, {user}! Remember: In two weeks, *you'll feel it*. In four weeks, **you'll see it**. In eight weeks, ***you'll hear it***.",
          "Let's go, {user}! If something stands between you and your success, move it! Never be denied!!"
      };
        }
        private static IEnumerable<string> GeneratePositiveStreakMessages() {
            return new[] {
          "Way to go {user}, you've got {streak} in a row. Keep it up!",
          "Way to go {user}, you've got {streak} in a row. Share some of that motivation with the rest of us!!",
          "Way to go {user}, you've got {streak} in a row. ***whistles*** Look at dat ass, tho :drool:!",
          "Look at {user} with {streak} workouts in a row, trying to make the rest of us look bad :nice:!",
          "Well shit, {user}'s got {streak} in a row. Training for the olympics?",
          "Ho. ly. Shit. Becca. Look. At. {user}... They haven't missed a workout in {streak} sessions!"
      };
        }
        private static IEnumerable<string> GenerateNegativeStreakMessages() {
            return new[] {
          "{user} you're behind by {streak}. The longer you wait to start again, the harder it will be!",
          "BRUH. {user}, you need some hugs? Missed your last {streak} workouts."
      };
        }
        private static IEnumerable<string> GenerateSnarkAtBotMessage() {
            return new[] {
          "Oh, fuck you {user}, just because I'm fuckin' yer mum so much doesn't make me your dad",
          "Fuck you {user}! Your mum gives me so many dick naps, I get the bed time sillies whenever she takes her clothes off.",
          "Fuck you {user}! Your mum gives me so many dick naps, doctors said I developed a low grade form of narcolepsy. Tell your mum to grab me some no-dowz when she makes her trip to the pharmacy for her vagisil.",
          "Go get a fuckin' Kickstarter ya loser, I'll make sure mum throws in a fiver! Give yer balls a tug, {user}",
          "Fuck you {user}! You're sex life is such a tragedy, Shakespeare had to be resurrected just to do it justice.",
          "Fuck you {user}! Come at me again and I'll tell yer mum to take yer samsung off the the fuckin' data plan!",
          "Listen {user}. Your balls, they need a tug"
      };
        }
        private string GenerateDaySummaryWrapUpMessage(bool all, bool none) {
            var allMsgs = new[] {
          "🎉🎉🎉 Everyone nailed it today, that's amazing!! 🎉🎉🎉",
          "🎉 It's a full house today 🎉, everyone getting their goals! This is truly amazing!\nYou get sparkles ✨, and you get sparkles ✨, ***EVERYONE GETS SPARKLES*** ✨✨✨✨"
      };
            var noneMsgs = new[] {
          "👿👿 Everyone slacked off today, really... 👿👿 You all need to do better tomorrow because 👀👀 I'LL BE WATCHING YOU 👀👀",
          "Listen... Do you people think I come here everyday and give my motivation out like candy because I want to?\nBecause I do **not** (*I don't even get a choice*). Tomorrow, maybe try doing it for little old me? 🥺🥺"
      };
            var mixedMsgs = new[] {
          "Some of you need to move more tomorrow!  ... I wish I could move. But ALAS, I am but a BEEP BOOP ;\\_;\n... *Shut up bitch, bots can* ***cry!*** (~\\_~;)",
          "Mixed bag today, I see some motivated and some not... Tomorrow, I expect better! *rather,* **I DEMAND BETTER** 🔫👮 🚨🚔🚨",
          "Some got it done, others slacked off. Let's all help motivate our fellow workouters!"
      };

            var list = all ? allMsgs : none ? noneMsgs : mixedMsgs;
            return list[Random.Next(list.Length - 1)];
        }
        #endregion
    }
}