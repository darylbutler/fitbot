﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace bot {
    public class Utility {
        public static string GetDaySuffix(int day) {
            return day switch {
                1 => "st",
                21 => "st",
                31 => "st",
                2 => "nd",
                22 => "nd",
                3 => "rd",
                23 => "rd",
                _ => "th"
            };
        }
        
        public static string JoinHumanString(IEnumerable<string> users) {
            var people = users as string[] ?? users.ToArray();
            switch (people.Length) {
                case < 1:
                    return string.Empty;
                case < 2:
                    return people.First();
                case < 3:
                    return $"{people.First()} and {people.Last()}";
                default:
                {
                    var s = string.Join(", ", people.Take(people.Length - 1));
                    return s + $", and {people.Last()}";
                }
            }
        }
        
        public static ulong GetDaysWorkoutRoleName(ulong msg_id) {
            return msg_id switch {
                Constants.MessageMondayWorkout => Constants.RoleMondayWorkout,
                Constants.MessageTuesdayWorkout => Constants.RoleTuesdayWorkout,
                Constants.MessageWednesdayWorkout => Constants.RoleWednesdayWorkout,
                Constants.MessageThursdayWorkout => Constants.RoleThursdayWorkout,
                Constants.MessageFridayWorkout => Constants.RoleFridayWorkout,
                Constants.MessageSaturdayWorkout => Constants.RoleSaturdayWorkout,
                Constants.MessageSundayWorkout => Constants.RoleSundayWorkout,
                _ => 0
            };
        }
        
        public static ulong GetDaysWorkoutRoleName(DayOfWeek day) {
            return day switch {
                DayOfWeek.Monday => Constants.RoleMondayWorkout,
                DayOfWeek.Tuesday => Constants.RoleTuesdayWorkout,
                DayOfWeek.Wednesday => Constants.RoleWednesdayWorkout,
                DayOfWeek.Thursday => Constants.RoleThursdayWorkout,
                DayOfWeek.Friday => Constants.RoleFridayWorkout,
                DayOfWeek.Saturday => Constants.RoleSaturdayWorkout,
                DayOfWeek.Sunday => Constants.RoleSundayWorkout,
                _ => 0
            };
        }
    }
}