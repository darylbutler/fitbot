using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Discord;
using Microsoft.Data.Sqlite;
using Serilog;

namespace bot {

    internal class Database {

        private readonly string _dbPath;

        public Database(string path) {
            _dbPath = path;
        }

        private SqliteConnection GetConnection() {
            var conn = new SqliteConnection($"Data Source={_dbPath}");
            conn.Open();
            return conn;
        }
        public void InitDatabase() {
            if (!File.Exists(_dbPath)) {
                Log.Warning("Database not found @ {dbPath} on init, will be created new", _dbPath);
                CreateDatabase();
                return;
            }

            // Database Exists, Check Version
            var ver = GetDatabaseVersion();
            if (ver == 1) {
                EnsureLatestDBVersion();
                return;
            }
            MigrateDBToVersion1();
        }

        #region ---- DB Versioninging and Upgrading -------------------------------------------------------------------
        private long GetDatabaseVersion() {
            using var conn = GetConnection();
            var cmd = conn.CreateCommand();

            cmd.CommandText = "SELECT version FROM db_version_log ORDER BY date DESC LIMIT 1";
            try {
                var val = cmd.ExecuteScalar();
                if (val is long dbVal)
                    return dbVal;
            } catch { /* ignored */
            }

            return 0L;
        }
        private void CreateDatabase() {
            EnsureLatestDBVersion();

            using var conn = GetConnection();
            var cmd = conn.CreateCommand();

            cmd.CommandText = @"INSERT INTO db_version_log VALUES (@day, @ver);";
            cmd.Parameters.AddWithValue("@day", DbDate(DateTime.Now));
            cmd.Parameters.AddWithValue("@ver", 1);
            cmd.ExecuteNonQuery();
        }
        private void EnsureLatestDBVersion() {
            using var conn = GetConnection();
            var cmd = conn.CreateCommand();

            cmd.CommandText = @"
                CREATE TABLE IF NOT EXISTS db_version_log (
                    date            TEXT UNIQUE NOT NULL,
                    version         INTEGER
                );

                CREATE TABLE IF NOT EXISTS status (
                    date            TEXT UNIQUE NOT NULL,   -- Date
                    msg_day_start   TEXT,                   -- ulong id of this day's DayStart Message
                    msg_day_end     TEXT,                   -- ulong id of this day's DaySummary Message
                    working_users	TEXT,					-- Comma-seperated list of ulong User IDs that were called to workout in the morning message
                    motivated_users TEXT                    -- Comma-seperated list of ulong User IDs of user's we motivated on this day
                );

                CREATE TABLE IF NOT EXISTS workouts (
                    id      INTEGER PRIMARY KEY,        -- Unique ID of this workout entry (Might not need)                    
                    date    TEXT NOT NULL,              -- Date
                    user_id TEXT NOT NULL,              -- ulong ID of user
                    missed  INTEGER,                    -- 1 if user did NOT workout this day
                    extra   INTEGER,                    -- 1 if user did workout today but it wasn't his goal to [workout this day]
                    msg_id  TEXT,                       -- ulong ID of the message on Discord
                    desc    TEXT,                       -- Workout description (Workout Message Content)
                    media   TEXT,                       -- Comma-seperated list of urls to images on discord's cdn (Workout Message Attachments)
                    UNIQUE (date, user_id)
                );
                
                CREATE TABLE IF NOT EXISTS user_options (
                    user_id         TEXT PRIMARY KEY,   -- ulong ID of user
                    workout_morn    INTEGER,            -- 1 if free to motivate user after morning
                    workout_lunch   INTEGER,            -- 1 if free to motivate user after lunch
                    workout_evening INTEGER,            -- free to motivate user after the afternoon
                    can_save_media  INTEGER,            -- If true, can download and save messages tagged with '#workout' attachments
                    can_motivate    INTEGER,            -- if true, can send motivation messages in chat
                    can_pos_streaks INTEGER,            -- If true, can notify if user is on a workout streak (ex, 5-days in a row without skipping workout)
                    can_neg_streaks INTEGER             -- If true, can notify if user has missed mulitple workouts in a row (ex, missed 3 workouts in a row)
                );
                CREATE UNIQUE INDEX IF NOT EXISTS user_options_unique ON user_options(user_id);
            ";
            cmd.ExecuteNonQuery();
        }
        private void MigrateDBToVersion1() {
            using var conn = GetConnection();
            var cmd = conn.CreateCommand();

            cmd.CommandText = @"
                BEGIN TRANSACTION;

                -- New Version Table
                CREATE TABLE IF NOT EXISTS db_version_log (
                    date            TEXT UNIQUE NOT NULL,
                    version         INTEGER
                );
                INSERT INTO db_version_log VALUES (@day, @ver);


                -- Migrate the status table
				DROP TABLE IF EXISTS status_new;
				CREATE TABLE status_new (
                    date            TEXT PRIMARY KEY,       -- Date
                    msg_day_start   TEXT,                   -- ulong id of this day's DayStart Message
                    msg_day_end     TEXT,                   -- ulong id of this day's DaySummary Message
					working_users	TEXT,					-- Comma-seperated list of ulong User IDs that were called to workout in the morning message
                    motivated_users TEXT                    -- Comma-seperated list of ulong User IDs of user's we motivated on this day
                );
				INSERT INTO status_new(date, msg_day_start, msg_day_end, motivated_users) SELECT date, msg_day_start, msg_day_end, motivated_users FROM status;
				ALTER TABLE status RENAME TO status_old;
				ALTER TABLE status_new RENAME TO status;
				COMMIT TRANSACTION;
            ";
            cmd.Parameters.AddWithValue("@day", DbDate(DateTime.Now));
            cmd.Parameters.AddWithValue("@ver", 1);
            cmd.ExecuteNonQuery();
        }
        #endregion

        #region ---- Backup -------------------------------------------------------------------------------------------
        public void BackupDatabase(string backupPath, bool manual = false) {
            // -- Create the backup
            if (!Directory.Exists(backupPath)) {
                Log.Error("Database: Backup path does not exist @ {path}!", backupPath);
                return;
            }

            var now = DateTime.Now;
            var newDBName = $"database_backup_{(manual ? "manual" : "auto")}_{now:yyyy-MM-dd_HH-mm-ss}.db";
            var path = Path.Combine(backupPath, newDBName);

            try {
                using var currentDB = GetConnection();
                using var backupConn = new SqliteConnection($"Data Source={path}");
                backupConn.Open();
                currentDB.BackupDatabase(backupConn);
                Log.Information("Database: Backup Completed and Successful");
            } catch (Exception ex) {
                Log.Error(ex, "Error in database backup!");
            }

            // -- Clean old backups
            BackupDatabase_CleanOldBackups(backupPath);
        }
        private static void BackupDatabase_CleanOldBackups(string backupPath) {
            var oldDate = DateTime.Now.AddDays(-60);
            var old = Directory.GetFiles(backupPath, "database_backup_auto_*").Where(x => Directory.GetCreationTime(x) < oldDate);
            foreach (var f in old) {
                Log.Information("Database: Removing old backup {backup}", Path.GetFileName(f));
                try { File.Delete(f); } catch { /* ignore */
                }
            }
        }
        #endregion

        #region ---- Status Table -------------------------------------------------------------------------------------
        public ulong GetWorkoutDayStartMessage(DateTime date) {
            using var conn = GetConnection();
            var cmd = conn.CreateCommand();

            cmd.CommandText = "SELECT msg_day_start FROM status WHERE date=@date;";
            cmd.Parameters.AddWithValue("@date", DbDate(date));
            var result = cmd.ExecuteScalar();

            if (result != null && ulong.TryParse(result.ToString(), out var id))
                return id;
            return 0;
        }
        public void SetWorkoutDayStartMessage(DateTime date, ulong id, IEnumerable<ulong> users) {
            using var conn = GetConnection();
            var cmd = conn.CreateCommand();

            cmd.CommandText = @"INSERT INTO status (date, msg_day_start, working_users)
                                VALUES(@date, @id, @users) 
                                ON CONFLICT(date) 
                                DO UPDATE SET msg_day_start=@id, working_users=@users;";

            cmd.Parameters.AddWithValue("@date", DbDate(date));
            cmd.Parameters.AddWithValue("@id", id);
            cmd.Parameters.AddWithValue("@users", string.Join(",", users));

            try {
                var rows = cmd.ExecuteNonQuery();
            } catch (Exception e) {
                Log.Error(e, "Error on Set Workout_DayStart Workout Message Id!");
            }
        }
        public ulong GetWorkoutDayEndMessage(DateTime date) {
            using var conn = GetConnection();
            var cmd = conn.CreateCommand();

            cmd.CommandText = "SELECT msg_day_end FROM status WHERE date=@date;";
            cmd.Parameters.AddWithValue("@date", DbDate(date));
            var result = cmd.ExecuteScalar();

            if (result != null && ulong.TryParse(result.ToString(), out var id))
                return id;
            return 0;
        }
        public void SetWorkoutDaySummaryMessage(DateTime date, ulong id) {
            using var conn = GetConnection();
            var cmd = conn.CreateCommand();

            cmd.CommandText = "UPDATE status SET msg_day_end=@id WHERE date=@date;";
            cmd.Parameters.AddWithValue("@date", DbDate(date));
            cmd.Parameters.AddWithValue("@id", id);

            try {
                var rows = cmd.ExecuteNonQuery();
            } catch (Exception e) {
                Log.Error(e, "Error on Set Workout_DayEnd Workout Message Id!");
            }
        }
        public List<ulong> GetDayStartWorkingOutUsers(DateTime date) {
            using var conn = GetConnection();
            var cmd = conn.CreateCommand();

            cmd.CommandText = "SELECT working_users FROM status WHERE date=@date;";
            cmd.Parameters.AddWithValue("@date", DbDate(date));
            var result = cmd.ExecuteScalar();

            var users = new List<ulong>();
            if (result is string r) {
                var rsplit = r.Split(',', StringSplitOptions.RemoveEmptyEntries);
                foreach (var u in rsplit)
                    if (ulong.TryParse(u, out var uid))
                        users.Add(uid);
            }
            return users;
        }
        public List<ulong> GetMotivatedUsers(DateTime date) {
            using var conn = GetConnection();
            var cmd = conn.CreateCommand();

            cmd.CommandText = $"SELECT motivated_users FROM status WHERE date='{DbDate(date)}';";
            var result = cmd.ExecuteScalar();

            var users = new List<ulong>();
            if (result is string r) {
                var rsplit = r.Split(',', StringSplitOptions.RemoveEmptyEntries);
                foreach (var u in rsplit)
                    if (ulong.TryParse(u, out var uid))
                        users.Add(uid);
            }
            return users;
        }
        public void AddMotivatedUser(DateTime date, ulong uid) {
            using var conn = GetConnection();
            var cmd = conn.CreateCommand();
            var uString = string.Empty;

            // Get
            cmd.CommandText = $"SELECT motivated_users FROM status WHERE date='{DbDate(date)}';";
            try {
                var result = cmd.ExecuteScalar();
                if (result != null && result is string r) uString = r;
            } catch (Exception e) {
                Log.Error(e, "Error Retrieving current day's Motivated User String");
            }

            // Modify
            if (uString.Length < 1)
                uString = uid.ToString();
            else
                uString = $"{uString},{uid}";

            // Store
            cmd.CommandText = $"UPDATE status SET motivated_users='{uString}' WHERE date='{DbDate(date)}';";
            try {
                cmd.ExecuteNonQuery();
            } catch (Exception e) {
                Log.Error(e, "Error saving updated User string");
            }
        }
        #endregion

        #region ---- Workout Table ------------------------------------------------------------------------------------
        private const string WorkoutViaEmoteKey = "[Emoted]";
        public void StoreEmotedWorkout(DateTime date, ulong uid, bool extra) {
            using var conn = GetConnection();
            var cmd = conn.CreateCommand();

            // INSERT OR IGNORE -- We always want to store workouts. If the emoji first, then tag msg, we want to store msg and not emoji
            cmd.CommandText = @"INSERT OR IGNORE INTO workouts (date, user_id, missed, extra, desc)
                                VALUES(@date, @uid, @missed, @extra, @desc);";
            cmd.Parameters.AddWithValue("@date", DbDate(date));
            cmd.Parameters.AddWithValue("@uid", uid);
            cmd.Parameters.AddWithValue("@missed", false);
            cmd.Parameters.AddWithValue("@extra", extra);
            cmd.Parameters.AddWithValue("@desc", WorkoutViaEmoteKey);

            try {
                var rows = cmd.ExecuteNonQuery();
            } catch (Exception e) {
                Log.Error(e, "Error on Store Emoted Workout, date: {date}, uid: {uid}, extra: {extra}", date, uid, extra);
            }
        }
        public void RemoveEmotedWorkout(DateTime date, ulong uid) {
            using var conn = GetConnection();
            var cmd = conn.CreateCommand();

            cmd.CommandText = @"DELETE FROM workouts WHERE date=@date AND user_id=@uid AND desc=@desc;";
            cmd.Parameters.AddWithValue("@date", DbDate(date));
            cmd.Parameters.AddWithValue("@uid", uid);
            cmd.Parameters.AddWithValue("@desc", WorkoutViaEmoteKey);

            try {
                var rows = cmd.ExecuteNonQuery();
            } catch (Exception e) {
                Log.Error(e, "Error on Delete Emoji Workout: date: {d}, uid: {uid}", date, uid);
            }
        }
        public void StoreTaggedWorkout(IMessage msg, bool extra) {
            using var conn = GetConnection();
            var cmd = conn.CreateCommand();

            var date = msg.Timestamp.LocalDateTime;
            var media = string.Empty;
            if (msg.Attachments.Count > 0) media = string.Join(",", msg.Attachments.Select(x => x.Url));

            // INSERT OR REPLACE -- We always want to store workouts. If the emoji first, then tag msg, we want to store msg and not emoji
            //      With replace, we will overwrite a message if, on creation, we are called, then on edit, we are called with same Msg ID#
            cmd.CommandText = @"INSERT OR REPLACE INTO workouts (date, user_id, missed, extra, msg_id, desc, media)
                            VALUES(@date, @uid, @missed, @extra, @mid, @msg, @media);";
            cmd.Parameters.AddWithValue("@date", DbDate(date));
            cmd.Parameters.AddWithValue("@uid", msg.Author.Id);
            cmd.Parameters.AddWithValue("@missed", false);
            cmd.Parameters.AddWithValue("@extra", extra);
            cmd.Parameters.AddWithValue("@mid", msg.Id);
            cmd.Parameters.AddWithValue("@msg", msg.Content);
            cmd.Parameters.AddWithValue("@media", media);

            try {
                var rows = cmd.ExecuteNonQuery();
            } catch (Exception e) {
                Log.Error(e, "Error on Store Workout With Message: date: {d}, msgId: {m}, extra: {e}, media: {media}", date, msg.Id, e, media);
            }
        }
        public bool RemoveTaggedWorkout(ulong mid) {
            using var conn = GetConnection();
            var cmd = conn.CreateCommand();

            cmd.CommandText = @"DELETE FROM workouts WHERE msg_id=@mid;";
            cmd.Parameters.AddWithValue("@mid", mid);

            try {
                var rows = cmd.ExecuteNonQuery();
                return rows > 0;
            } catch (Exception e) {
                Log.Error(e, "Error on Delete Tagged Workout: mid: {mid}", mid);
            }
            return false;
        }
        public void StoreMissedWorkout(DateTime date, ulong uid) {
            using var conn = GetConnection();
            var cmd = conn.CreateCommand();

            cmd.CommandText = @"INSERT OR IGNORE INTO workouts (date, user_id, missed)
                                VALUES(@date, @uid, @missed);";
            cmd.Parameters.AddWithValue("@date", DbDate(date));
            cmd.Parameters.AddWithValue("@uid", uid);
            cmd.Parameters.AddWithValue("@missed", true);

            try {
                var rows = cmd.ExecuteNonQuery();
            } catch (Exception e) {
                Log.Error(e, "Error on Store Missed Workout, date: {date}, uid: {uid}", date, uid);
            }
        }
        public List<ulong> GetUsersWhoHaveWorkoutedOut(DateTime date) {
            using var conn = GetConnection();
            var cmd = conn.CreateCommand();
            var users = new List<ulong>();

            cmd.CommandText = "SELECT user_id FROM workouts WHERE missed=@missed AND date=@date;";
            cmd.Parameters.AddWithValue("@date", DbDate(date));
            cmd.Parameters.AddWithValue("@missed", false);

            try {
                var reader = cmd.ExecuteReader();

                while (reader.Read())
                    if (ulong.TryParse(reader.GetString(0), out var uid))
                        users.Add(uid);
            } catch (Exception e) {
                Log.Error(e, "Error on get user who has worked out on date: {date}, usersGotAlready: {users}", date, string.Join(", ", users));
            }

            return users;
        }
        public Dictionary<ulong, List<(DateTime Date, bool missed, bool extra)>> GetWorkoutOverview(DateTime start, DateTime end) {
            using var conn = GetConnection();
            var cmd = conn.CreateCommand();
            var log = new Dictionary<ulong, List<(DateTime Date, bool missed, bool extra)>>();

            cmd.CommandText = "SELECT date,user_id,missed,extra FROM workouts WHERE date >= @start AND date < @end ORDER BY date DESC;";
            cmd.Parameters.AddWithValue("@start", DbDate(start));
            cmd.Parameters.AddWithValue("@end", DbDate(end));

            try {
                using var reader = cmd.ExecuteReader();
                while (reader.Read()) {
                    if (reader["date"] is not string date || reader["user_id"] is not string user_id || !ulong.TryParse(user_id, out var uid))
                        continue;

                    var missed = reader["missed"] as long? ?? 0;
                    var extra = reader["extra"] as long? ?? 0;

                    if (!log.ContainsKey(uid)) log[uid] = new List<(DateTime Date, bool missed, bool extra)>();

                    var entry = (DbDate(date), missed > 0, extra > 0);
                    log[uid].Add(entry);
                }
            } catch (Exception e) {
                Log.Error(e, "Error on get workout overview between {s} and {e}", start, end);
            }

            return log;
        }
        public List<(DateTime Date, bool missed, bool extra)> GetUsersWorkoutOverview(ulong uid, DateTime start, DateTime end) {
            using var conn = GetConnection();
            var cmd = conn.CreateCommand();
            var log = new List<(DateTime Date, bool missed, bool extra)>();

            cmd.CommandText = "SELECT date,missed,extra FROM workouts WHERE user_id=@uid AND date >= @start AND date < @end ORDER BY date DESC;";
            cmd.Parameters.AddWithValue("@uid", uid);
            cmd.Parameters.AddWithValue("@start", DbDate(start));
            cmd.Parameters.AddWithValue("@end", DbDate(end));

            try {
                using var reader = cmd.ExecuteReader();
                while (reader.Read()) {
                    if (reader["date"] is not string date)
                        continue;

                    var missed = reader["missed"] as long? ?? 0;
                    var extra = reader["extra"] as long? ?? 0;

                    var entry = (DbDate(date), missed > 0, extra > 0);
                    log.Add(entry);
                }
            } catch (Exception e) {
                Log.Error(e, "Error on get user's workout overview for {u} between {s} and {e}", uid, start, end);
            }

            return log;
        }

        public struct WorkoutEntry {
            public DateTime Date;
            public ulong UserId;
            public bool Missed, Extra;
            public ulong MsgId;
            public string Description, Media;
        }
        public List<WorkoutEntry> GetUserWorkouts(ulong uid, DateTime start, DateTime end, int limit = 28) {
            using var conn = GetConnection();
            var cmd = conn.CreateCommand();
            var log = new List<WorkoutEntry>();

            cmd.CommandText = $"SELECT * FROM workouts WHERE user_id=@uid AND date >= @start AND date < @end ORDER BY date DESC LIMIT {limit};";
            cmd.Parameters.AddWithValue("@uid", uid);
            cmd.Parameters.AddWithValue("@start", start);
            cmd.Parameters.AddWithValue("@end", end);

            try {
                var reader = cmd.ExecuteReader();

                while (reader.Read())
                    log.Add(new WorkoutEntry {
                        Date = DbDate(reader["date"].ToString()),
                        UserId = uid,
                        MsgId = ulong.TryParse(reader["msg_id"].ToString(), out var mid) ? mid : 0,
                        Missed = reader["missed"] is not DBNull && (long)reader["missed"] > 0,
                        Extra = reader["extra"] is not DBNull && (long)reader["extra"] > 0,
                        Description = reader["desc"].ToString() == WorkoutViaEmoteKey ? "" : reader["desc"].ToString(),
                        Media = reader["media"].ToString()
                    });
            } catch (Exception e) {
                Log.Error(e, "Error on get workout log for user {u} between {s} and {e}, got {i} entries from db before error",
                    uid, DbDate(start), DbDate(end), log.Count);
            }
            log.Sort((a, b) => a.Date.CompareTo(b.Date));

            return log;
        }
        #endregion

        #region ---- User Options -------------------------------------------------------------------------------------
        private void SetUserOption(ulong uid, string key, bool value) {
            using var conn = GetConnection();
            var cmd = conn.CreateCommand();

            cmd.CommandText = $@"INSERT INTO user_options (user_id, {key})
                                VALUES(@uid, @value) 
                                ON CONFLICT(user_id) 
                                DO UPDATE SET {key}=@value;";

            cmd.Parameters.AddWithValue("@uid", uid);
            cmd.Parameters.AddWithValue("@value", value);

            try {
                var rows = cmd.ExecuteNonQuery();
            } catch (Exception e) {
                Log.Error(e, "Error on Update User Options for {key}, value: {value}", key, value);
            }
        }
        public void SetUserWorkoutMorningPerson(ulong uid, bool val) {
            SetUserOption(uid, "workout_morn", val);
        }
        public void SetUserWorkoutLunchPerson(ulong uid, bool val) {
            SetUserOption(uid, "workout_lunch", val);
        }
        public void SetUserWorkoutEveningPerson(ulong uid, bool val) {
            SetUserOption(uid, "workout_evening", val);
        }
        public void SetUserCanMotivate(ulong uid, bool val) {
            SetUserOption(uid, "can_motivate", val);
        }
        public void SetUserPositiveStreak(ulong uid, bool val) {
            SetUserOption(uid, "can_pos_streaks", val);
        }
        public void SetUserNegitiveStreak(ulong uid, bool val) {
            SetUserOption(uid, "can_neg_streaks", val);
        }

        public List<ulong> GetUsers() {
            using var conn = GetConnection();
            var cmd = conn.CreateCommand();
            var users = new List<ulong>();

            cmd.CommandText = "SELECT user_id FROM user_options;";

            try {
                var reader = cmd.ExecuteReader();

                while (reader.Read())
                    if (ulong.TryParse(reader.GetString(0), out var uid))
                        users.Add(uid);
            } catch (Exception e) {
                Log.Error(e, "Error on get users");
            }

            return users;
        }
        private List<ulong> GetUsersWithOptions(string key, bool value) {
            using var conn = GetConnection();
            var cmd = conn.CreateCommand();
            var users = new List<ulong>();

            cmd.CommandText = $"SELECT user_id FROM user_options WHERE {key}=@value;";
            cmd.Parameters.AddWithValue("@value", value);

            try {
                var reader = cmd.ExecuteReader();

                while (reader.Read())
                    if (ulong.TryParse(reader.GetString(0), out var uid))
                        users.Add(uid);
            } catch (Exception e) {
                Log.Error(e, "Error on get [multiple] users with options for {key} with value: {v}", key, value);
            }

            return users;
        }
        public List<ulong> GetUsersWhoWorkoutInMorning() {
            return GetUsersWithOptions("workout_morn", true);
        }
        public List<ulong> GetUsersWhoWorkoutAtLunch() {
            return GetUsersWithOptions("workout_lunch", true);
        }
        public List<ulong> GetUsersWhoWorkoutInEvening() {
            return GetUsersWithOptions("workout_evening", true);
        }
        public List<ulong> GetUsersWhoAllowMotivate() {
            return GetUsersWithOptions("can_motivate", true);
        }
        public List<ulong> GetUsersWhoAllowPositiveStreaks() {
            return GetUsersWithOptions("can_pos_streaks", true);
        }
        public List<ulong> GetUsersWhoAllowNegitiveStreaks() {
            return GetUsersWithOptions("can_neg_streaks", true);
        }

        private bool GetUserHasOption(ulong uid, string key, bool value) {
            using var conn = GetConnection();
            var cmd = conn.CreateCommand();
            var users = new List<ulong>();

            cmd.CommandText = $"SELECT {key} FROM user_options WHERE user_id='{uid}';";

            try {
                var reader = cmd.ExecuteScalar();
                if (reader != null) {
                    if (value)
                        return (int)reader == 1;
                    return (int)reader == 0;
                }
            } catch (Exception e) {
                Log.Error(e, "Error on get [single] user with option for {key} with value: {v}", key, value);
            }
            return false;
        }
        public bool GetUsersWhoWorkoutInMorning(ulong uid) {
            return GetUserHasOption(uid, "workout_morn", true);
        }
        public bool GetUsersWhoWorkoutInLunch(ulong uid) {
            return GetUserHasOption(uid, "workout_lunch", true);
        }
        public bool GetUsersWhoWorkoutInEvening(ulong uid) {
            return GetUserHasOption(uid, "workout_evening", true);
        }
        public bool GetUsersWhoAllowMotivate(ulong uid) {
            return GetUserHasOption(uid, "can_motivate", true);
        }
        public bool GetUsersWhoAllowPositiveStreaks(ulong uid) {
            return GetUserHasOption(uid, "can_pos_streaks", true);
        }
        public bool GetUsersWhoAllowNegitiveStreaks(ulong uid) {
            return GetUserHasOption(uid, "can_neg_streaks", true);
        }
        #endregion

        #region ---- Utility ------------------------------------------------------------------------------------------
        private static string DbDate(DateTime date) {
            return $"{date:yyyy-MM-dd}";
        }
        private static DateTime DbDate(string date) {
            return DateTime.TryParse(date, out var dt) ? dt : DateTime.MinValue;
        }
        #endregion
    }
}